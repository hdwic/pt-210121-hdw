'use strict';

const { query } = require("express");

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('ClassMembers', [
      {
        memberId: 1,
        classId: 1,
      },
      {
        memberId: 2,
        classId: 1,
      },
      {
        memberId: 1,
        classId: 4,
      },
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('ClassMembers', null, {});
  }
};
