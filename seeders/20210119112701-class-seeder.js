'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Classes', [
      {
        name: 'UMPTN-A',
        schedule: '2021-03-10',
        isOpen: true,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'UMPTN-B',
        schedule: '2021-03-17',
        isOpen: true,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Architecture 101',
        schedule: '2021-01-09',
        isOpen: false,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Mechanic 101',
        schedule: '2021-02-09',
        isOpen: true,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Classes', null, {});
  }
};
