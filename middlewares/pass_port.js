const passport = require('passport');
const passportJWT = require('passport-jwt');

const JwtStrategy = passportJWT.Strategy;
const ExtractJwt = passportJWT.ExtractJwt;

passport.use(new JwtStrategy({
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  // jwtFromRequest: ExtractJwt.fromHeader('authorization'),
  secretOrKey: process.env.SECRETKEY
  // secretOrKey: keys.jwt.secret

},
  function (jwtPayload, done) {
    try {
      return done(null, jwtPayload.payLoad);
    } catch (error) {
      return done(error);
    }
  }));