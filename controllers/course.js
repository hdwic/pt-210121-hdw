const { Member, Class } = require('../models');

module.exports = {
    async courseList(req, res) {
        try {
            //this below code for showing all member include association of 'course' and 'class'
            // class is included via pivot table of classmember
            // include call for association via aliases and written in quotes!!
            const member = await Member.findAll(
                {
                   
                    include: [ 'course',    // course is included direct as alias and no other attribute and without calling the model    
                        {                   //  since member and course has direct association defined in the model
                            model: Class, 
                            as: 'classSchedule', 
                            attributes: ["id", "name", "schedule", "isOpen"],
                                through: {
                                    attributes: [],
                            }
                        }

                    ]
                }
            );
            res.status(200).send(member);
        }
        catch (err) {
            res.status(500).send(err);
        }
    }
}