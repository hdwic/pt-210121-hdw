const { Member, Class } = require('../models');

class MemberControllerOOP  {
    async memberList(req, res) {
        try {
            const member = await Member.findAll(
                {
                    include: ['course',
                    {
                        model: Class, 
                        as: 'classSchedule', 
                        attributes: ["id", "name", "schedule", "isOpen"],
                        through: {
                            attributes: [],
                        }
                    }
                ]
                }
            );
            res.status(200).send(member);
        } catch (err) {
            res.status(500).send(err);
        }
    }

    async createMember(req, res){
        try{ 
            const cekEmail = await Member.findOne({ where: { email: req.body.email } });
            if (cekEmail === null){
                const createdMember = await Member.create({
                    name: req.body.name,
                    mobile: req.body.mobile,
                    courseId: req.body.courseId,
                    email: req.body.email,
                    password: req.body.password
                })
                res.status(201).json({
                    status: true,
                    message: 'Member Created!',
                    data: { createdMember }
                }) 
        
            } else {
                res.status(406).json({
                    status: false,
                    message: `${req.body.email} already exist, use other email`,
                })
            }
        
        } 
        catch (err) {
            res.json(err)
        }

    }

    async updateMember(req, res){
        try {
            const foundMember = await Member.findByPk(req.params.id);
          //   console.log(foundAuthor)
            if(foundMember){
              const updatedMember = await Member.update({
                  name : req.body.name,
                  email: req.body.email,
                }, {
                  where: {
                    id: req.params.id
                  }
                });
                res.status(202).json({
                    status: true,
                    message: `Member with ID ${req.params.id} successfuly updated`,
                    updated: {updatedMember}
                })
            } else {
              res.status(400).json({
                  status: false,
                  message: `Member with ID ${req.params.id} not found`
              })
            }
          }
          catch (err) {
              res.json(err)
          }
    }

    async deleteMember(req, res) {
        try {
    
            const foundMember = await Member.findByPk(req.params.id);
            if(foundMember){
                const deletedMember = await Member.destroy( { where: { id: req.params.id} } )
                res.status(202).json({
                    status: true,
                    message: `Member with ID ${req.params.id} successfuly deleted`,
                    updated: {deletedMember}
            })
            } else {
                res.status(400).json({
                    status: false,
                    message: `Can not found Member with ID ${req.params.id}`,
                  })
            }
            
        } catch (err) {
            res.json(err)
          };
    }
    
}

module.exports = MemberControllerOOP