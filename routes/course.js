var express = require('express');
var router = express.Router();

const courseController = require('../controllers/course')
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/api/course', courseController.courseList)

module.exports = router;
