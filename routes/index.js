var express = require('express');
var router = express.Router();

// const memberController = require('../controllers/member')
const loginController = require('../controllers/login')
const passport = require('passport');
// require('../middlewares/pass_port');
const MemberController = require('../controllers/MemberControllers')
const memberController = new MemberController

/* GET home page. */

router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/api/member', passport.authenticate('jwt', { 'session': false }), memberController.memberList)

router.post('/api/member', memberController.createMember)

router.put('/api/member/:id', passport.authenticate('jwt', { 'session': false }), memberController.updateMember)

router.delete('/api/member/:id', passport.authenticate('jwt', { 'session': false }), memberController.deleteMember)

router.post('/api/login', loginController.login);


module.exports = router;
